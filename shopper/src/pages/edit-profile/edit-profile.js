var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { App, NavController, NavParams, LoadingController, AlertController, ToastController, ModalController, ActionSheetController, Platform } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { TabsPage } from '../tabs/tabs';
import { ModalPreferencePage } from '../modal-preference/modal-preference';
import { StorageProvider } from '../../providers/storage/storage';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Transfer } from '@ionic-native/transfer';
import { LoginPage } from '../login/login';
var EditProfilePage = /** @class */ (function () {
    function EditProfilePage(navCtrl, navParams, http, loadingCtrl, builder, alertCtrl, toastCtrl, rutebaseApi, modalCtrl, storage, actionSheetCtrl, camera, file, platform, filePath, transfer, app) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.builder = builder;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.rutebaseApi = rutebaseApi;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.file = file;
        this.platform = platform;
        this.filePath = filePath;
        this.transfer = transfer;
        this.app = app;
        this.user = {};
        this.municipios = [];
        this.ciudades = [];
        this.colonias = [];
        this.dataPreference = [];
        this.createSuccess = false;
        this.preferences = [];
        this.sexos = [{ id: 0, tipo: 'Femenino' }, { id: 1, tipo: 'Masculino' }];
        this.city = 'Municipio';
        this.formErrors = {
            'nombre': '',
            'telefono': '',
            'email': '',
            'ciudad': '',
            'estado': ''
        };
        this.lastImage = null;
        this.image_user = 'assets/imgs/user.png';
        this.user = this.storage.getObject('userShopper');
        this.initForm();
    }
    EditProfilePage.prototype.initForm = function () {
        var _this = this;
        this.registerUserForm = this.builder.group({
            nombre: [this.user.nombre, [Validators.required, Validators.minLength(2)]],
            email: [this.user.email, [Validators.required, Validators.email]],
            edad: [this.user.cliente.edad, [Validators.required]],
            sexo: [this.user.cliente.sexo],
            estado_id: [this.user.cliente.estado_id],
            municipio_id: [this.user.cliente.municipio_id],
            localidad_id: [this.user.cliente.localidad_id],
            imagen: [this.user.cliente.imagen],
            preferencias: ['']
        });
        this.image_user = this.user.cliente.imagen;
        this.preferences = this.user.cliente.preferencias;
        this.registerUserForm.valueChanges.subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged();
        this.http.get(this.rutebaseApi.getRutaApi() + 'categorias')
            .toPromise()
            .then(function (data) {
            _this.dataPreference = data;
            for (var i = 0; i < _this.dataPreference.categorias.length; ++i) {
                for (var j = 0; j < _this.preferences.length; ++j) {
                    if (_this.dataPreference.categorias[i].id == _this.preferences[j].id) {
                        _this.dataPreference.categorias[i].checked = true;
                    }
                }
            }
            _this.page();
        }, function (msg) {
            _this.page();
            _this.presentToast('No se pudo cargar las preferencias, ingresa de nuevo', 1500);
        });
    };
    EditProfilePage.prototype.page = function () {
        var _this = this;
        this.showLoadingc();
        this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/estados')
            .toPromise()
            .then(function (data) {
            _this.datos = data;
            _this.estados = _this.datos.estados;
            _this.setEstado(_this.registerUserForm.value.estado_id);
            _this.loading.dismiss();
        }, function (msg) {
            _this.presentToast('No se pudo cargar los estados y ciudades, ingresa de nuevo', 1500);
            _this.loading.dismiss();
        });
    };
    EditProfilePage.prototype.setEstado = function (estado) {
        this.municipios = [];
        this.colonias = [];
        this.initEstados(estado);
        if (estado == 9) {
            this.city = 'Delegación';
        }
        else {
            this.city = 'Municipio';
        }
    };
    EditProfilePage.prototype.initEstados = function (estado_id) {
        var _this = this;
        this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/municipios?estado_id=' + estado_id)
            .toPromise()
            .then(function (data) {
            _this.datos1 = data;
            _this.municipios = _this.datos1.municipios;
            _this.http.get(_this.rutebaseApi.getRutaApi() + 'mx/get/localidades?municipio_id=' + _this.registerUserForm.value.municipio_id)
                .toPromise()
                .then(function (data) {
                _this.datos2 = data;
                _this.colonias = _this.datos2.localidades;
            }, function (msg) {
                _this.presentToast('No se pudo cargar las colonias, intenta de nuevo', 1500);
            });
        }, function (msg) {
            _this.presentToast('No se pudo cargar los municipios, intenta de nuevo', 1500);
        });
    };
    EditProfilePage.prototype.setMunicipio = function (event) {
        var _this = this;
        this.colonias = [];
        this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/localidades?municipio_id=' + event)
            .toPromise()
            .then(function (data) {
            _this.datos2 = data;
            _this.colonias = _this.datos2.localidades;
            _this.registerUserForm.patchValue({ localidad_id: _this.colonias[0].id });
        }, function (msg) {
            _this.presentToast('No se pudo cargar las colonias, intenta de nuevo', 1500);
        });
    };
    EditProfilePage.prototype.setColonia = function (event) {
        this.registerUserForm.patchValue({ localidad_id: event });
    };
    EditProfilePage.prototype.presentModal = function () {
        var _this = this;
        if (this.dataPreference != '') {
            var profileModal = this.modalCtrl.create(ModalPreferencePage, { prefer: this.preferences, collec: this.dataPreference.categorias });
            profileModal.onDidDismiss(function (data) {
                _this.preferences = data;
            });
            profileModal.present();
        }
        else {
            this.presentToast('No se pudieron cargar las preferencias ingresa de nuevo', 1500);
        }
    };
    EditProfilePage.prototype.deleteChip = function (prefer) {
        var index = this.preferences.findIndex(function (item1) { return item1.nombre === prefer.nombre; });
        if (index !== -1) {
            this.preferences.splice(index, 1);
        }
        for (var i = 0; i < this.dataPreference.categorias.length; ++i) {
            if (this.dataPreference.categorias[i].nombre == prefer.nombre) {
                this.dataPreference.categorias[i].checked = false;
            }
        }
    };
    EditProfilePage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Seleccione una Imagen',
            buttons: [
                {
                    text: 'Cargar de Librería',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Usar Camara',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    EditProfilePage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            _this.presentToast('Error al seleccionar la imagen', 1500);
        });
    };
    // Create a new name for the image
    EditProfilePage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    EditProfilePage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            _this.uploadImage();
        }, function (error) {
            _this.presentToast('Error al guardar la imagen', 1500);
        });
    };
    // Always get the accurate path to your apps folder
    EditProfilePage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    EditProfilePage.prototype.uploadImage = function () {
        var _this = this;
        // Destination URL
        var url = "http://shopper.internow.com.mx/images_uploads/upload_clientes.php";
        // File for Upload
        var targetPath = this.pathForImage(this.lastImage);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename }
        };
        var fileTransfer = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'Subiendo Imagen...',
            spinner: 'ios'
        });
        this.loading.present();
        // Use the FileTransfer to upload the image
        fileTransfer.upload(targetPath, url, options).then(function (data) {
            _this.loading.dismissAll();
            _this.image_user = data.response;
        }, function (err) {
            _this.loading.dismissAll();
            _this.presentToast('Error al subir la imagen', 1500);
            _this.image_user = _this.user.cliente.imagen;
        });
    };
    EditProfilePage.prototype.editProfile = function () {
        var _this = this;
        this.registerUserForm.patchValue({ email: this.registerUserForm.value.email.toLowerCase() });
        this.registerUserForm.patchValue({ imagen: this.image_user });
        this.registerUserForm.patchValue({ preferencias: JSON.stringify(this.preferences) });
        if (this.registerUserForm.valid) {
            this.showLoading();
            this.http.put(this.rutebaseApi.getRutaApi() + 'clientes/' + this.user.cliente.id, this.registerUserForm.value)
                .toPromise()
                .then(function (data) {
                _this.datosU = data;
                var storage = _this.storage.getObject('userShopper');
                storage.cliente = _this.datosU.cliente;
                _this.storage.setObject('userShopper', storage);
                _this.loading.dismissAll();
                _this.presentToast('Usuario actualizado con éxito', 1500);
                _this.navCtrl.pop();
            }, function (msg) {
                _this.loading.dismiss();
                if (msg.status == 400 || msg.status == 401) {
                    _this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo', 1500);
                    _this.app.getRootNav().setRoot(LoginPage);
                }
                else {
                    _this.presentToast(msg.error.error, 1500);
                }
            });
        }
        else {
            this.validateAllFormFields(this.registerUserForm);
            this.presentToast('¡Todos los campos deben estar completos!', 1500);
        }
    };
    EditProfilePage.prototype.onValueChanged = function (data) {
        if (!this.registerUserForm) {
            return;
        }
        var form = this.registerUserForm;
        for (var field in this.formErrors) {
            var control = form.get(field);
            this.formErrors[field] = '';
            if (control && control.dirty && !control.valid) {
                for (var key in control.errors) {
                    this.formErrors[field] += true;
                    console.log(key);
                }
            }
        }
    };
    EditProfilePage.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsDirty({ onlySelf: true });
                _this.onValueChanged();
            }
            else if (control instanceof FormGroup) {
                _this.validateAllFormFields(control);
            }
        });
    };
    EditProfilePage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Editando perfil...',
            spinner: 'ios',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    EditProfilePage.prototype.showLoadingc = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Cargando...',
            spinner: 'ios'
        });
        this.loading.present();
    };
    EditProfilePage.prototype.showPopup = function (title, text) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons: [
                {
                    text: 'OK',
                    handler: function (data) {
                        if (_this.createSuccess) {
                            _this.navCtrl.setRoot(TabsPage);
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    EditProfilePage.prototype.presentToast = function (text, time) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: time,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    EditProfilePage = __decorate([
        Component({
            selector: 'page-edit-profile',
            templateUrl: 'edit-profile.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, HttpClient, LoadingController, FormBuilder, AlertController, ToastController, RuteBaseProvider, ModalController, StorageProvider, ActionSheetController, Camera, File, Platform, FilePath, Transfer, App])
    ], EditProfilePage);
    return EditProfilePage;
}());
export { EditProfilePage };
//# sourceMappingURL=edit-profile.js.map