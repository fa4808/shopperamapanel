import { Component } from '@angular/core';
import { App, NavController, NavParams, LoadingController, Loading, AlertController, ToastController, ModalController, ActionSheetController, Platform, Keyboard } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { TabsPage } from '../tabs/tabs';
import { ModalPreferencePage } from '../modal-preference/modal-preference';
import { StorageProvider } from '../../providers/storage/storage';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { LoginPage } from '../login/login';

declare var cordova: any;

@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

	user: any = {};
	loading: Loading;
	public registerUserForm: FormGroup;
	public estados: any;
	public municipios: any = [];
	public ciudades: any = [];
	public colonias: any = [];
	public datos: any;
	public datos1: any;
	public datos2: any;
	public datos3: any;
  public datosU: any;
	public dataPreference: any = [];
	public createSuccess: boolean = false;
	public preferences: any = [];
	sexos = [{id:0, tipo: 'Femenino'}, {id:1, tipo: 'Masculino'}];
	city: string = 'Municipio';
	formErrors = {
		'nombre': '',
		'telefono': '',
		'email': '',
		'ciudad': '',
		'estado': ''
	};
  public lastImage: string = null;
  public image_user: string = 'assets/imgs/user.png';
  public input: string = '';
  public countries: string[] = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private http: HttpClient, 
    private loadingCtrl: LoadingController, 
    private builder: FormBuilder, 
    public alertCtrl: AlertController, 
    private toastCtrl: ToastController, 
    private rutebaseApi: RuteBaseProvider, 
    public modalCtrl: ModalController, 
    public storage: StorageProvider, 
    public actionSheetCtrl: ActionSheetController, 
    private camera: Camera, 
    private file: File, 
    private platform: Platform, 
    private filePath: FilePath, 
    private transfer: Transfer, 
    public app: App,
    private keyboard: Keyboard
    ) {
		this.user = this.storage.getObject('userShopper');
    console.log(this.user)
		this.initForm();
	}

  initForm() {
    this.registerUserForm = this.builder.group({
      nombre: [this.user.nombre, [Validators.required, Validators.minLength(2)]],
      email: [this.user.email, [Validators.required, Validators.email]],
      edad: [this.user.cliente.edad, [Validators.required]],
      sexo: [this.user.cliente.sexo],
      estado_id: [this.user.cliente.estado_id], 
      municipio_id: [this.user.cliente.municipio_id], 
      localidad_id: [this.user.cliente.localidad_id],
      imagen: [this.user.cliente.imagen],
      preferencias: [''],
      input: ['']
    });
    this.image_user = this.user.cliente.imagen;
    this.preferences = this.user.cliente.preferencias;

    this.registerUserForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
    this.http.get(this.rutebaseApi.getRutaApi() + 'categorias')
    .toPromise()
    .then(
    data => {
      this.dataPreference = data;
      for (var i = 0; i < this.dataPreference.categorias.length; ++i) {
        for (var j = 0; j < this.preferences.length; ++j) {
          if (this.dataPreference.categorias[i].id == this.preferences[j].id) {
            this.dataPreference.categorias[i].checked = true;
          } 
        }
      }
      this.page();
    },
    msg => {
      this.page();
      this.presentToast('No se pudo cargar las preferencias, ingresa de nuevo',1500);
    });
  }

  page(){
    this.showLoadingc();
    this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/estados')
    .toPromise()
    .then(
      data => {
        this.datos = data;
        this.estados = this.datos.estados; 
        this.initEstados(this.registerUserForm.value.estado_id);
        if (this.registerUserForm.value.estado_id == 9) {
          this.city = 'Delegación';
        } else {
          this.city = 'Municipio';
        }
        this.loading.dismiss();
      },
      msg => {
        this.presentToast('No se pudo cargar los estados y ciudades, ingresa de nuevo',1500);
        this.loading.dismiss();
    });
  }

  setEstado(estado){
    this.municipios = [];
    this.colonias = [];
    this.registerUserForm.patchValue({estado_id: estado});
    if (estado == 9) {
      this.city = 'Delegación';
    } else {
      this.city = 'Municipio';
    }
    this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/municipios?estado_id='+estado)
    .toPromise()
    .then(
      data => {
        console.log(data)
        this.datos1 = data;
        this.municipios = this.datos1.municipios;
        this.registerUserForm.patchValue({municipio_id: this.municipios[0].id});
         this.setMunicipio(this.municipios[0].id);
      },
      msg => {
        this.presentToast('No se pudo cargar los municipios, intenta de nuevo',1500);
    });
  }

  initEstados(estado_id){
    this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/municipios?estado_id='+estado_id)
    .toPromise()
    .then(
      data => {
        console.log(data)
        this.datos1 = data;
        this.municipios = this.datos1.municipios;
        this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/localidades?municipio_id='+this.registerUserForm.value.municipio_id)
        .toPromise()
        .then(
          data => {
            this.datos2 = data;
            this.colonias = this.datos2.localidades;
            setTimeout(()=>{
              for (var i = 0; i < this.colonias.length; ++i) {
                if (this.colonias[i].id == this.registerUserForm.value.localidad_id) {
                  this.registerUserForm.patchValue({input: this.colonias[i].nombre});
                }
              }
            },1500);
          },
          msg => {
            this.presentToast('No se pudo cargar las colonias, intenta de nuevo',1500);
        });
      },
      msg => {
        this.presentToast('No se pudo cargar los municipios, intenta de nuevo',1500);
    });
  }

  setMunicipio(event){
    this.colonias = [];
    this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/localidades?municipio_id='+event)
    .toPromise()
    .then(
      data => {
        this.datos2 = data;
        this.colonias = this.datos2.localidades;
        this.registerUserForm.patchValue({localidad_id: this.colonias[0].id});
        this.registerUserForm.patchValue({input: this.colonias[0].nombre});
      },
      msg => {
        this.presentToast('No se pudo cargar las colonias, intenta de nuevo',1500);
    });
  }

  setColonia(event){
    this.registerUserForm.patchValue({localidad_id: event});
  }

  search() {
    if (!this.registerUserForm.value.input.trim().length || !this.keyboard.isOpen()) {
      this.countries = [];
      return;
    }
    
    this.countries = this.colonias.filter(item => item.nombre.toUpperCase().includes(this.registerUserForm.value.input.toUpperCase()));
  }

  add(item) {
    this.registerUserForm.patchValue({input: item.nombre});
    this.registerUserForm.patchValue({localidad_id: item.id});
    this.countries = [];
  }

  removeFocus() {
    this.keyboard.close();
  }

  presentModal() {
    if(this.dataPreference != ''){
      let profileModal = this.modalCtrl.create(ModalPreferencePage, { prefer: this.preferences, collec: this.dataPreference.categorias });
      profileModal.onDidDismiss(data => {
       this.preferences = data;
      });
      profileModal.present();
    } else {
      this.presentToast('No se pudieron cargar las preferencias ingresa de nuevo',1500);
    } 
  }

  deleteChip(prefer){
    let index = this.preferences.findIndex((item1) => item1.nombre === prefer.nombre);
    if(index !== -1){
      this.preferences.splice(index, 1);
    }
    for (var i = 0; i < this.dataPreference.categorias.length; ++i) {
      if (this.dataPreference.categorias[i].nombre == prefer.nombre) {
        this.dataPreference.categorias[i].checked = false;
      } 
    }
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Seleccione una Imagen',
      buttons: [
        {
          text: 'Cargar de Librería',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Usar Camara',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
   
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error al seleccionar la imagen',1500);
    });
  }

  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";
    return newFileName;
  }
   
  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      this.uploadImage();
    }, error => {
      this.presentToast('Error al guardar la imagen',1500);
    });
  }
   
  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  public uploadImage() {
    // Destination URL
    var url = "http://shopper.internow.com.mx/images_uploads/upload_clientes.php";
   
    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);
   
    // File name only
    var filename = this.lastImage;
   
    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'fileName': filename}
    };
   
    const fileTransfer: TransferObject = this.transfer.create();
   
    this.loading = this.loadingCtrl.create({
      content: 'Subiendo Imagen...',
      spinner: 'ios'
    });
    this.loading.present();
    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
      this.loading.dismissAll()
      this.image_user = data.response;
    }, err => {
      this.loading.dismissAll();
      this.presentToast('Error al subir la imagen',1500);
      this.image_user = this.user.cliente.imagen;
    });
  }

  editProfile(){
    console.log(this.registerUserForm.value)
    this.registerUserForm.patchValue({email: this.registerUserForm.value.email.toLowerCase()});
    this.registerUserForm.patchValue({imagen: this.image_user});
    this.registerUserForm.patchValue({preferencias: JSON.stringify(this.preferences)});
    if (this.registerUserForm.valid) {
      this.showLoading();
      this.http.put(this.rutebaseApi.getRutaApi() + 'clientes/'+ this.user.cliente.id+'?token='+this.storage.get('tokenShopper'), this.registerUserForm.value)
      .toPromise()
      .then(
        data => {
          this.datosU = data;
          console.log(data)
          let storage = this.storage.getObject('userShopper');
          storage.cliente = this.datosU.cliente;
          this.storage.setObject('userShopper', storage);
          this.loading.dismissAll();
          this.presentToast('Usuario actualizado con éxito',1500);
          this.navCtrl.pop();
        },
        msg => {
          this.loading.dismiss();
          if(msg.status == 400 || msg.status == 401){ 
            this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo',1500);
            this.app.getRootNav().setRoot(LoginPage);
          } else {
            this.presentToast(msg.error.error,1500);
          }
      });
    } else {
      this.validateAllFormFields(this.registerUserForm);
      this.presentToast('¡Todos los campos deben estar completos!',1500);
    }    
  }

  onValueChanged(data?: any) {
    if (!this.registerUserForm) { return; }
    const form = this.registerUserForm;
    for (const field in this.formErrors) { 
      const control = form.get(field);
      this.formErrors[field] = '';
      if (control && control.dirty && !control.valid) {
        for (const key in control.errors) {
          this.formErrors[field] += true;
          console.log(key);
        }
      } 
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsDirty({ onlySelf:true });
        this.onValueChanged();
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Editando perfil...',
      spinner: 'ios',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showLoadingc() {
    this.loading = this.loadingCtrl.create({
      content: 'Cargando...',
      spinner: 'ios'
    });
    this.loading.present();
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (this.createSuccess) {
              this.navCtrl.setRoot(TabsPage);
            }
          }
        }
      ]
    });
    alert.present();
  }

  presentToast(text,time) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: time,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
