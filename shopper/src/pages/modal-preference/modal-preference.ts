import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

@Component({
  selector: 'page-modal-preference',
  templateUrl: 'modal-preference.html',
})
export class ModalPreferencePage {
	collection: any = []; 
    preference: any = [];
    active: any = [];

	constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public platform: Platform, public storage: StorageProvider) {
		this.active = navParams.get('prefer');
		this.collection = navParams.get('collec');
		this.initList(this.active);	
	}

  	backButtonAction(){
  		this.viewCtrl.dismiss(this.preference); 
  	}

	initList(active){
		for (var i = 0; i < this.collection.length; ++i) {
			for (var j = 0; j < active.length; ++j) {
				if (this.collection[i].id == active[j].id) {
					this.preference.push(active[j]);
				} 
			}
		}
	}

 	onChange(event,item){
 		if (event) {
 			this.preference.push(item);
 		} else {
 			let index = this.preference.findIndex((item1) => item1.id === item.id);
	        if(index !== -1){
	          this.preference.splice(index, 1);
	        }
 		};
 	}

    dismiss() {
	   this.viewCtrl.dismiss(this.preference);
	}

}
