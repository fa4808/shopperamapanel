var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { PollPage } from '../poll/poll';
import { TabsPage } from '../tabs/tabs';
var AcceptQuestionPage = /** @class */ (function () {
    function AcceptQuestionPage(navCtrl, navParams, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.logo_sucursal = '';
        this.nombre_sucursal = '';
        this.usuario = {
            nombre: '',
            imagen: 'assets/imgs/user.png'
        };
        this.logo_sucursal = this.navParams.get('logo');
        this.nombre_sucursal = this.navParams.get('nombre');
        this.cuestionario_id = this.navParams.get('cuestionario_id');
        this.usuario.nombre = this.storage.getObject('userShopper').nombre;
        this.usuario.imagen = this.storage.getObject('userShopper').cliente.imagen;
    }
    AcceptQuestionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AcceptQuestionPage');
    };
    AcceptQuestionPage.prototype.acceptQuestion = function () {
        this.navCtrl.push(PollPage, { cuestionario_id: this.cuestionario_id, logo: this.logo_sucursal, nombre: this.nombre_sucursal });
    };
    AcceptQuestionPage.prototype.declineQuestion = function () {
        this.navCtrl.setRoot(TabsPage);
    };
    AcceptQuestionPage = __decorate([
        Component({
            selector: 'page-accept-question',
            templateUrl: 'accept-question.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, StorageProvider])
    ], AcceptQuestionPage);
    return AcceptQuestionPage;
}());
export { AcceptQuestionPage };
//# sourceMappingURL=accept-question.js.map