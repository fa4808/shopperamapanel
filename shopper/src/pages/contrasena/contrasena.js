var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { LoginPage } from '../login/login';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
var ContrasenaPage = /** @class */ (function () {
    function ContrasenaPage(navCtrl, navParams, toastCtrl, http, loadingCtrl, builder, rutebaseAPI) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.builder = builder;
        this.rutebaseAPI = rutebaseAPI;
        this.updateSuccess = false;
        this.formErrors = {
            'password': '',
            'rpassword': '',
            'token': ''
        };
        this.validationMessages = {
            'password': {
                'required': 'La contraseña es requerida.',
                'minlength': 'La contraseña debe contener un mínimo de 8 caracteres.'
            },
            'rpassword': {
                'required': 'El confirmar contraseña es requerido.',
                'minlength': 'La contraseña debe contener un mínimo de 8 caracteres.'
            }
        };
        this.id_cliente = navParams.get('id_cliente');
        this.UpdatePassword = this.builder.group({
            password: ['', [Validators.required, Validators.minLength(8)]],
            rpassword: ['', [Validators.required, Validators.minLength(8)]],
            token: [navParams.get('token')]
        });
        this.UpdatePassword.valueChanges.subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged();
    }
    ContrasenaPage.prototype.updatePassword = function () {
        var _this = this;
        if (this.UpdatePassword.valid) {
            if (this.UpdatePassword.value.password !== this.UpdatePassword.value.rpassword) {
                this.presentToast("Contraseñas no coinciden");
            }
            else {
                this.showLoading();
                this.http.put(this.rutebaseAPI.getRutaApi() + 'usuarios/' + this.id_cliente, this.UpdatePassword.value)
                    .toPromise()
                    .then(function (data) {
                    _this.loading.dismiss();
                    _this.navCtrl.setRoot(LoginPage);
                }, function (msg) {
                    var err = msg.error;
                    _this.loading.dismiss();
                    _this.presentToast(err.error);
                });
            }
        }
        else {
            this.validateAllFormFields(this.UpdatePassword);
        }
    };
    ContrasenaPage.prototype.onValueChanged = function (data) {
        if (!this.UpdatePassword) {
            return;
        }
        var form = this.UpdatePassword;
        for (var field in this.formErrors) {
            var control = form.get(field);
            this.formErrors[field] = '';
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    };
    ContrasenaPage.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsDirty({ onlySelf: true });
                _this.onValueChanged();
            }
            else if (control instanceof FormGroup) {
                _this.validateAllFormFields(control);
            }
        });
    };
    ContrasenaPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Actualizando contraseña...',
            spinner: 'ios',
            duration: 25000,
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    ContrasenaPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    ContrasenaPage = __decorate([
        Component({
            selector: 'page-contrasena',
            templateUrl: 'contrasena.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ToastController, HttpClient, LoadingController, FormBuilder, RuteBaseProvider])
    ], ContrasenaPage);
    return ContrasenaPage;
}());
export { ContrasenaPage };
//# sourceMappingURL=contrasena.js.map