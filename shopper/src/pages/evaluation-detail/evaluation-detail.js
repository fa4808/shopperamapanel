var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { ElementsServicesProvider } from '../../providers/elements-services/elements-services';
import { LoginPage } from '../login/login';
import { PhotoViewer } from '@ionic-native/photo-viewer';
var EvaluationDetailPage = /** @class */ (function () {
    function EvaluationDetailPage(navCtrl, navParams, http, rutebaseAPI, elementService, app, photoViewer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.rutebaseAPI = rutebaseAPI;
        this.elementService = elementService;
        this.app = app;
        this.photoViewer = photoViewer;
        this.cuestionario_id = '';
        this.logo_sucursal = 'http://shopper.internow.com.mx/images/flats.png';
        this.nombre_sucursal = '';
        this.sucursal_direccion = '';
        this.descripcion = '';
        this.nombre = '';
        this.cuestionario = [];
        this.respuesta_id = '';
        this.img_factura = '';
        this.monto_pagado = '';
        this.respuesta_id = navParams.get('respuesta_id');
        this.initEvaluations();
    }
    EvaluationDetailPage.prototype.initEvaluations = function () {
        var _this = this;
        this.elementService.showLoading('Cargando Evaluaciones...');
        this.http.get(this.rutebaseAPI.getRutaApi() + 'evaluaciones/' + this.respuesta_id)
            .toPromise()
            .then(function (data) {
            _this.datos = data;
            console.log(data);
            _this.elementService.hideLoading();
            _this.cuestionario = JSON.parse(_this.datos.respuesta.cuestionario);
            _this.descripcion = _this.datos.respuesta.cuest.descripcion;
            _this.nombre = _this.datos.respuesta.cuest.nombre;
            _this.logo_sucursal = _this.datos.respuesta.sucursal.logo;
            _this.nombre_sucursal = _this.datos.respuesta.sucursal.nombre;
            _this.sucursal_direccion = _this.datos.respuesta.sucursal.direccion;
            _this.img_factura = _this.datos.respuesta.imagen_factura;
            _this.monto_pagado = _this.datos.respuesta.cuest.pagoxcuest;
        }, function (msg) {
            _this.elementService.hideLoading();
            if (msg.status == 400 || msg.status == 401) {
                _this.elementService.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
                _this.app.getRootNav().setRoot(LoginPage);
            }
            else {
                _this.elementService.presentToast(msg.error.error);
            }
        });
    };
    EvaluationDetailPage.prototype.viewTicket = function () {
        this.photoViewer.show(this.img_factura);
    };
    EvaluationDetailPage = __decorate([
        Component({
            selector: 'page-evaluation-detail',
            templateUrl: 'evaluation-detail.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, HttpClient, RuteBaseProvider, ElementsServicesProvider, App, PhotoViewer])
    ], EvaluationDetailPage);
    return EvaluationDetailPage;
}());
export { EvaluationDetailPage };
//# sourceMappingURL=evaluation-detail.js.map