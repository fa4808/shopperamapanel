import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { ElementsServicesProvider } from '../../providers/elements-services/elements-services';
import { LoginPage } from '../login/login';
import { ImageViewerController } from 'ionic-img-viewer';
import { StorageProvider } from '../../providers/storage/storage';

@Component({
  selector: 'page-evaluation-detail',
  templateUrl: 'evaluation-detail.html',
})
export class EvaluationDetailPage {

	public cuestionario_id: string = '';
	public logo_sucursal: string = 'http://shopper.internow.com.mx/images/flats.png';
	public nombre_sucursal: string = '';
	public sucursal_direccion: string = '';
	public descripcion: string = '';
	public nombre: string = '';
	public datos: any;
	public cuestionario: any[] = [];
	public respuesta_id: string = '';
	public img_factura: string = '';
	public monto_pagado: string = '';
  public motivo: string = '';
  public estado: number = 1;
  _imageViewerCtrl: ImageViewerController;
  @ViewChild('imageViewer') input: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public storage: StorageProvider, public rutebaseAPI: RuteBaseProvider,public elementService: ElementsServicesProvider, public app: App, imageViewerCtrl: ImageViewerController) {
  	this.respuesta_id = navParams.get('respuesta_id');
    this._imageViewerCtrl = imageViewerCtrl;
  	this.initEvaluations();
  }

  initEvaluations(){
  	this.elementService.showLoading('Cargando Evaluaciones...');
    this.http.get(this.rutebaseAPI.getRutaApi()+'evaluaciones/'+this.respuesta_id+'?token='+this.storage.get('tokenShopper'))
    .toPromise()
    .then(
    data => {
      this.datos = data;
      console.log(data);
      this.elementService.hideLoading();
      this.cuestionario = JSON.parse(this.datos.respuesta.cuestionario);
      this.descripcion = this.datos.respuesta.cuest.descripcion;
      this.nombre = this.datos.respuesta.cuest.nombre;
      this.logo_sucursal = this.datos.respuesta.sucursal.logo;
      this.nombre_sucursal = this.datos.respuesta.sucursal.nombre;
      this.sucursal_direccion = this.datos.respuesta.sucursal.direccion;
      this.img_factura = this.datos.respuesta.imagen_factura;
      this.monto_pagado = this.datos.respuesta.cuest.pagoxcuest;
      this.motivo = this.datos.respuesta.comentario;
      this.estado = this.datos.respuesta.estado;
    },
    msg => {
      this.elementService.hideLoading();
      if(msg.status == 400 || msg.status == 401){ 
        this.elementService.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
        this.app.getRootNav().setRoot(LoginPage);
      } else {
        this.elementService.presentToast(msg.error.error);
      }
    });
  }

  viewTicket(){
    const imageViewer = this._imageViewerCtrl.create(this.input.nativeElement);
    imageViewer.present();
  }

}
