var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { App, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { ElementsServicesProvider } from '../../providers/elements-services/elements-services';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';
import { EvaluationDetailPage } from '../evaluation-detail/evaluation-detail';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
var EvaluationsPendingPage = /** @class */ (function () {
    function EvaluationsPendingPage(navCtrl, navParams, http, rutebaseAPI, elementService, storage, app) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.rutebaseAPI = rutebaseAPI;
        this.elementService = elementService;
        this.storage = storage;
        this.app = app;
        this.evaluations = [];
        this.item = [];
        this.searchTerm = '';
        this.searching = false;
        this.preferences = [];
        this.user = this.storage.getObject('userShopper');
        this.client_id = this.user.cliente.id;
        this.searchControl = new FormControl();
    }
    EvaluationsPendingPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.initEvaluations();
        this.searchControl.valueChanges.debounceTime(700).subscribe(function (search) {
            _this.searching = false;
            _this.setFilteredItems();
        });
    };
    EvaluationsPendingPage.prototype.onSearchInput = function () {
        this.searching = true;
    };
    EvaluationsPendingPage.prototype.setFilteredItems = function () {
        this.evaluations = this.filterItems(this.searchTerm);
    };
    EvaluationsPendingPage.prototype.filterItems = function (searchTerm) {
        if (searchTerm != '') {
            var vihicledata_1 = this.item;
            var items_1 = [];
            var array = Object.keys(vihicledata_1).map(function (k) {
                return vihicledata_1[k];
            });
            for (var i = 0; i < array.length; i++) {
                array[i].filter(function (item) {
                    if (item.cuest.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                        items_1.push(item);
                    }
                    else {
                        if (item.sucursal.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                            items_1.push(item);
                        }
                    }
                });
            }
            return this.convertTo(items_1, 'created_at', true);
        }
        else {
            return this.item;
        }
    };
    EvaluationsPendingPage.prototype.initEvaluations = function () {
        var _this = this;
        this.elementService.showLoading('Cargando Evaluaciones...');
        this.http.get(this.rutebaseAPI.getRutaApi() + 'evaluaciones/respondidas/' + this.client_id)
            .toPromise()
            .then(function (data) {
            console.log(data);
            _this.datos = data;
            //this.item = this.datos.respuestas;
            //console.log(this.item);
            _this.item = _this.convertTo(_this.datos.respuestas, 'created_at', true);
            console.log(_this.item);
            _this.setFilteredItems();
            _this.elementService.hideLoading();
            _this.getPreference();
        }, function (msg) {
            _this.elementService.hideLoading();
            if (msg.status == 400 || msg.status == 401) {
                _this.elementService.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
                _this.app.getRootNav().setRoot(LoginPage);
            }
            else {
                _this.elementService.presentToast(msg.error.error);
            }
        });
    };
    EvaluationsPendingPage.prototype.convertTo = function (arr, key, dayWise) {
        var groups = {};
        for (var i = 0; i < arr.length; i++) {
            if (dayWise) {
                if (arr[i][key].indexOf(':') > -1) {
                    arr[i][key] = new Date(arr[i][key]).toLocaleDateString();
                }
            }
            else {
                arr[i][key] = new Date(arr[i][key]).toTimeString();
            }
            groups[arr[i][key]] = groups[arr[i][key]] || [];
            groups[arr[i][key]].push(arr[i]);
        }
        return groups;
    };
    ;
    EvaluationsPendingPage.prototype.getPreference = function () {
        var _this = this;
        this.http.get(this.rutebaseAPI.getRutaApi() + 'clientes/' + this.client_id)
            .toPromise()
            .then(function (data) {
            _this.datos = data;
            console.log(_this.datos);
            _this.preferences = _this.datos.cliente.preferencias;
        }, function (msg) {
            _this.elementService.hideLoading();
            if (msg.status == 400 || msg.status == 401) {
                _this.elementService.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
                _this.app.getRootNav().setRoot(LoginPage);
            }
            else {
                _this.elementService.presentToast(msg.error.error);
                console.log(_this.evaluations.length);
            }
        });
    };
    EvaluationsPendingPage.prototype.evaluationDetail = function (evaluation) {
        console.log(evaluation);
        this.navCtrl.push(EvaluationDetailPage, { respuesta_id: evaluation.id });
    };
    EvaluationsPendingPage = __decorate([
        Component({
            selector: 'page-evaluations-pending',
            templateUrl: 'evaluations-pending.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, HttpClient, RuteBaseProvider, ElementsServicesProvider, StorageProvider, App])
    ], EvaluationsPendingPage);
    return EvaluationsPendingPage;
}());
export { EvaluationsPendingPage };
//# sourceMappingURL=evaluations-pending.js.map