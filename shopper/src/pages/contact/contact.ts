import { LoginPage } from '../login/login';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App, ModalController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { StorageProvider } from '../../providers/storage/storage';
import { Facebook } from '@ionic-native/facebook';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { WalletPage } from '../wallet/wallet';
import { HelpPage } from '../help/help';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
	public user: any;
	public usuario = {
	    nombre: '',
	    imagen: 'assets/imgs/user.png'
	};
	public datos: any;
	public account_pedert: string = '';
	public client_id: string = '';
	public count_pending: number = 0;
	public count_answer: number = 0;
  	public band_chatSupport: boolean = false;
	public chat_support = {
		admin_id: '',
		chat_id: '',
		token_notificacion: ''
	};

	constructor(public navCtrl: NavController, public app: App, public http: HttpClient, public navParams: NavParams, private alertCtrl: AlertController, public modalCtrl: ModalController, public auth: AuthServiceProvider, private rutebaseApi: RuteBaseProvider, public storage: StorageProvider, private facebook: Facebook) {
	}

	ionViewWillEnter(){
    	this.user = this.storage.getObject('userShopper');
    	this.usuario.nombre = this.user.nombre;
    	this.usuario.imagen = this.user.cliente.imagen;
    	this.client_id = this.user.cliente.id;
    	this.getCount();
  	}

	getCount(){
		this.http.get(this.rutebaseApi.getRutaApi() + 'clientes/estadistica/'+this.client_id+'?token='+this.storage.get('tokenShopper'))
	    .toPromise()
	    .then(
	      data => {
	        this.datos = data;
	        this.count_pending = this.datos.count_pendientes;
	        this.count_answer = this.datos.count_respondidas;
	        this.getIds();
	      },
	      msg => {
	      	this.getIds();
	    });
	};

	getIds(){
	  	this.http.get(this.rutebaseApi.getRutaApi()+'chats/clientes/michat/'+this.user.id+'?token='+this.storage.get('tokenShopper'))
	    .toPromise()
	    .then(
	      data => {
	        this.datos = data;
	        this.chat_support.admin_id = this.datos.chat.admin_id;
	        this.chat_support.chat_id = this.datos.chat.id;
	        this.chat_support.token_notificacion = this.datos.admin[0].token_notificacion;
	        this.band_chatSupport = true;
	      },
	      msg => {
	      	if(msg.status == 404){ 
	          this.band_chatSupport = true;
	          this.chat_support.admin_id = msg.error.admin[0].id;
	          this.chat_support.token_notificacion = msg.error.admin[0].token_notificacion;
	        } else if(msg.status == 409){
	          this.band_chatSupport = false;
	        }
		});
	}
	
  	logout(){
  		this.facebook.getLoginStatus()
		.then(rta => {
		  if(rta.status == 'connected'){
		  	this.facebook.logout()
		    .then(rta => {
		      	this.storage.set('tokenShopper','');
				this.storage.setObject('userShopper', '');
		      	this.app.getRootNav().setRoot(LoginPage);
		    })
		    .catch(error =>{
		      this.storage.set('tokenShopper','');
				this.storage.setObject('userShopper', '');
		      	this.app.getRootNav().setRoot(LoginPage);
		    });
		  } else {
		  	this.storage.set('tokenShopper','');
			this.storage.setObject('userShopper', '');
		    this.app.getRootNav().setRoot(LoginPage);
		  }
		})
		.catch(error =>{
		  	this.storage.set('tokenShopper','');
			this.storage.setObject('userShopper', '');
		    this.app.getRootNav().setRoot(LoginPage);
		});
	}

	editProfile(){
		this.navCtrl.push(EditProfilePage);
	}

	viewWallet(){
		this.navCtrl.push(WalletPage);
	}

	viewHelp(){
		if (this.band_chatSupport) {
			this.navCtrl.push(HelpPage, {admin_id: this.chat_support.admin_id, chat_id: this.chat_support.chat_id, token_notificacion: this.chat_support.token_notificacion});
		}
	}

	presentConfirm() {
	  let alert = this.alertCtrl.create({
	    title: 'Cerrar Sesión',
	    message: '¿Deseas salir de la aplicación?',
	    buttons: [
	      {
	        text: 'No',
	        role: 'cancel',
	        handler: () => {
	          console.log('Cancel clicked');
	        }
	      },
	      {
	        text: 'Si',
	        handler: () => {
	          this.logout();
	        }
	      }
	    ]
	  });
	  alert.present();
	}

}
