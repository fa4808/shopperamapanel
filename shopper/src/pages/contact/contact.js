var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { LoginPage } from '../login/login';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App, ModalController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { StorageProvider } from '../../providers/storage/storage';
import { Facebook } from '@ionic-native/facebook';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { WalletPage } from '../wallet/wallet';
var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl, app, http, navParams, alertCtrl, modalCtrl, auth, rutebaseApi, storage, facebook) {
        this.navCtrl = navCtrl;
        this.app = app;
        this.http = http;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.auth = auth;
        this.rutebaseApi = rutebaseApi;
        this.storage = storage;
        this.facebook = facebook;
        this.usuario = {
            nombre: '',
            imagen: 'assets/imgs/user.png'
        };
        this.account_pedert = '';
        this.client_id = '';
        this.count_pending = 0;
        this.count_answer = 0;
    }
    ContactPage.prototype.ionViewWillEnter = function () {
        this.user = this.storage.getObject('userShopper');
        this.usuario.nombre = this.user.nombre;
        this.usuario.imagen = this.user.cliente.imagen;
        this.client_id = this.user.cliente.id;
        this.getCount();
    };
    ContactPage.prototype.getCount = function () {
        var _this = this;
        this.http.get(this.rutebaseApi.getRutaApi() + 'clientes/estadistica/' + this.client_id)
            .toPromise()
            .then(function (data) {
            _this.datos = data;
            _this.count_pending = _this.datos.count_pendientes;
            _this.count_answer = _this.datos.count_respondidas;
        }, function (msg) {
        });
    };
    ;
    ContactPage.prototype.logout = function () {
        var _this = this;
        this.facebook.getLoginStatus()
            .then(function (rta) {
            if (rta.status == 'connected') {
                _this.facebook.logout()
                    .then(function (rta) {
                    _this.storage.set('tokenShopper', '');
                    _this.storage.setObject('userShopper', '');
                    _this.app.getRootNav().setRoot(LoginPage);
                })
                    .catch(function (error) {
                    _this.storage.set('tokenShopper', '');
                    _this.storage.setObject('userShopper', '');
                    _this.app.getRootNav().setRoot(LoginPage);
                });
            }
            else {
                _this.storage.set('tokenShopper', '');
                _this.storage.setObject('userShopper', '');
                _this.app.getRootNav().setRoot(LoginPage);
            }
        })
            .catch(function (error) {
            _this.storage.set('tokenShopper', '');
            _this.storage.setObject('userShopper', '');
            _this.app.getRootNav().setRoot(LoginPage);
        });
    };
    ContactPage.prototype.editProfile = function () {
        this.navCtrl.push(EditProfilePage);
    };
    ContactPage.prototype.viewWallet = function () {
        this.navCtrl.push(WalletPage);
    };
    ContactPage.prototype.presentConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Cerrar Sesión',
            message: '¿Deseas salir de la aplicación?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Si',
                    handler: function () {
                        _this.logout();
                    }
                }
            ]
        });
        alert.present();
    };
    ContactPage = __decorate([
        Component({
            selector: 'page-contact',
            templateUrl: 'contact.html'
        }),
        __metadata("design:paramtypes", [NavController, App, HttpClient, NavParams, AlertController, ModalController, AuthServiceProvider, RuteBaseProvider, StorageProvider, Facebook])
    ], ContactPage);
    return ContactPage;
}());
export { ContactPage };
//# sourceMappingURL=contact.js.map