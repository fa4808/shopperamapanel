import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, App, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { ElementsServicesProvider } from '../../providers/elements-services/elements-services';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-modal-account',
  templateUrl: 'modal-account.html',
})

export class ModalAccountPage {

	public registerAccountForm: FormGroup;
	public cliente_id: string = '';
	public user: any;
	public datos: any;
  	public datos1: any;
  	public datos2: any;
  	public datos3: any;
	formErrors = {
		'holder_name': '',
		'alias': '',
		'clabe': ''
	};
	private create_account = {
	    token_id: '',
	    nombre_titular: '',
	    cuenta: '',
	    cliente_id: '',
	    predeterminado: 2
	}
	private update_client = {
		customer_id: ''
	}

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams, 
		private http: HttpClient, 
		private builder: FormBuilder, 
		public viewCtrl: ViewController, 
		public elementService: ElementsServicesProvider, 
		public rutebaseAPI: RuteBaseProvider, 
		public storage: StorageProvider,
		public app: App,
    	private toastCtrl: ToastController) {
		this.user = this.storage.getObject('userShopper');
		this.cliente_id = this.user.cliente.id;
		this.update_client.customer_id = navParams.get('token_id');
		this.create_account.predeterminado = navParams.get('predeterminate');
		this.initForm();
	}

  	backButtonAction(){
  		this.viewCtrl.dismiss(); 
  	}

	initForm() {
		this.registerAccountForm = this.builder.group({
		  holder_name: ['', [Validators.required, Validators.minLength(2)]],
		  clabe: ['', [Validators.required, Validators.maxLength(45)]],
		  token: [this.storage.get('tokenShopper')]
		});
		this.registerAccountForm.valueChanges.subscribe(data => this.onValueChanged(data));
		this.onValueChanged();
		this.registerAccountForm.controls['clabe'].valueChanges.subscribe(
	      (selectedValue) => {
	      	if (selectedValue) {
	      		if (selectedValue.toString().length > 18) {
		          this.registerAccountForm.patchValue({clabe: selectedValue.toString().substr(0, 18)});
		        }; 
	      	}; 
	      }
	    );
	}

	dismiss() {
		this.viewCtrl.dismiss();
	}

	addAccount(registerAccount){
		if (this.registerAccountForm.valid) {
			this.elementService.showLoading('Enviando información...')
	      	this.saveCard();
	    } else {
	      this.validateAllFormFields(this.registerAccountForm);
	      this.elementService.presentToast('¡Faltan datos para el registro de la cuenta!');
	    };
	};

	saveCard(){
		this.create_account.cliente_id = this.cliente_id;
		this.http.post(this.rutebaseAPI.getRutaApi()+'pagos/openpay/customers/'+this.update_client.customer_id+'/bankaccounts', this.registerAccountForm.value)
		.toPromise()
		.then(
		data => {
		  this.datos1 = data;
		  if (this.datos1.openpay.http_code == 409) {
		  	this.elementService.loading.dismissAll();
		  	if (this.datos1.openpay.error_code == 2001) {
		  		this.presentToast('La cuenta bancaria ya está asociada.');
		  	}
		  } else {
		  	  this.create_account.token_id = this.datos1.openpay.id;
			  this.create_account.nombre_titular = this.registerAccountForm.get('holder_name').value;
			  this.create_account.cuenta = this.datos1.openpay.clabe;
			  this.http.post(this.rutebaseAPI.getRutaApi()+'cuentas?token='+this.storage.get('tokenShopper'), this.create_account)
			  .toPromise()
			  .then(
			  data => {
			  	this.elementService.loading.dismissAll();
			    this.elementService.presentToast('Cuenta Bancaria agregada con éxito');
			    this.dismiss();
			  },
			  msg => {
			    this.elementService.loading.dismissAll();
			    if(msg.status == 400 || msg.status == 401){ 
		          this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
		          this.app.getRootNavs()[0].setRoot(LoginPage);
		        } else {
		          this.presentToast(msg.error.error);
		        }
			  });
		  }
		},
		msg => {
		    this.elementService.loading.dismissAll();
		    this.elementService.presentToast(msg.error.description);
		    if(msg.status == 400 || msg.status == 401){ 
	          this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
	          this.app.getRootNavs()[0].setRoot(LoginPage);
	        } else {
	          this.presentToast(msg.error.error);
	        }
		});
	}

	onValueChanged(data?: any) {
		if (!this.registerAccountForm) { return; }
		const form = this.registerAccountForm;

		for (const field in this.formErrors) { 
		  const control = form.get(field);
		  this.formErrors[field] = '';
		  if (control && control.dirty && !control.valid) {
		    for (const key in control.errors) {
		      this.formErrors[field] += true;
		      console.log(key);
		    }
		  } 
		}
	}

	validateAllFormFields(formGroup: FormGroup) {
		Object.keys(formGroup.controls).forEach(field => {
		  const control = formGroup.get(field);
		  if (control instanceof FormControl) {
		    control.markAsDirty({ onlySelf:true });
		    this.onValueChanged();
		  } else if (control instanceof FormGroup) {
		    this.validateAllFormFields(control);
		  }
		});
	}

	presentToast(text) {
	    let toast = this.toastCtrl.create({
	      message: text,
	      duration: 3000,
	      position: 'top'
	    });

	    toast.onDidDismiss(() => {
	      console.log('Dismissed toast');
	    });

	    toast.present();
	}
}
