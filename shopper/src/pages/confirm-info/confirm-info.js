var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController, ModalController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { TabsPage } from '../tabs/tabs';
import { ModalPreferencePage } from '../modal-preference/modal-preference';
var ConfirmInfoPage = /** @class */ (function () {
    function ConfirmInfoPage(navCtrl, auth, navParams, http, loadingCtrl, builder, alertCtrl, toastCtrl, rutebaseApi, modalCtrl) {
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.builder = builder;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.rutebaseApi = rutebaseApi;
        this.modalCtrl = modalCtrl;
        this.user = {};
        this.municipios = [];
        this.ciudades = [];
        this.colonias = [];
        this.dataPreference = [];
        this.createSuccess = false;
        this.preferences = [];
        this.sexos = [{ id: 0, tipo: 'Femenino' }, { id: 1, tipo: 'Masculino' }];
        this.city = 'Municipio';
        this.formErrors = {
            'nombre': '',
            'telefono': '',
            'email': '',
            'ciudad': '',
            'estado': ''
        };
        this.user = navParams.get('user');
        this.initForm();
    }
    ConfirmInfoPage.prototype.initForm = function () {
        var _this = this;
        this.registerUserForm = this.builder.group({
            nombre: [this.user.nombre, [Validators.required, Validators.minLength(2)]],
            email: [this.user.email, [Validators.required, Validators.email]],
            edad: ['', [Validators.required]],
            tipo_registro: [this.user.tipo_registro],
            sexo: ['Femenino'],
            estado_id: [''],
            municipio_id: [''],
            localidad_id: [''],
            imagen: [this.user.imagen],
            preferencias: [''],
            id_facebook: [this.user.id_facebook],
            id_twitter: [this.user.id_twitter],
            id_instagram: [this.user.id_instagram],
            token_notificacion: [this.user.token_notificacion]
        });
        this.registerUserForm.valueChanges.subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged();
        this.http.get(this.rutebaseApi.getRutaApi() + 'categorias')
            .toPromise()
            .then(function (data) {
            _this.dataPreference = data;
            _this.page();
        }, function (msg) {
            _this.page();
            _this.presentToast('No se pudo cargar las preferencias, ingresa de nuevo', 1500);
        });
    };
    ConfirmInfoPage.prototype.page = function () {
        var _this = this;
        this.showLoadingc();
        this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/estados')
            .toPromise()
            .then(function (data) {
            _this.datos = data;
            _this.estados = _this.datos.estados;
            _this.registerUserForm.patchValue({ estado_id: _this.estados[0].id });
            _this.setEstado(_this.estados[0].id);
            _this.loading.dismiss();
        }, function (msg) {
            _this.presentToast('No se pudo cargar los estados y ciudades, ingresa de nuevo', 1500);
            _this.loading.dismiss();
        });
    };
    ConfirmInfoPage.prototype.setEstado = function (estado) {
        this.municipios = [];
        this.colonias = [];
        this.initEstados(estado);
        if (estado == 9) {
            this.city = 'Delegación';
        }
        else {
            this.city = 'Municipio';
        }
    };
    ConfirmInfoPage.prototype.initEstados = function (estado_id) {
        var _this = this;
        this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/municipios?estado_id=' + estado_id)
            .toPromise()
            .then(function (data) {
            _this.datos1 = data;
            _this.municipios = _this.datos1.municipios;
            _this.registerUserForm.patchValue({ municipio_id: _this.municipios[0].id });
            _this.http.get(_this.rutebaseApi.getRutaApi() + 'mx/get/localidades?municipio_id=' + _this.municipios[0].id)
                .toPromise()
                .then(function (data) {
                _this.datos2 = data;
                _this.colonias = _this.datos2.localidades;
                _this.registerUserForm.patchValue({ localidad_id: _this.colonias[0].id });
            }, function (msg) {
                _this.presentToast('No se pudo cargar las colonias, intenta de nuevo', 1500);
            });
        }, function (msg) {
            _this.presentToast('No se pudo cargar los municipios, intenta de nuevo', 1500);
        });
    };
    ConfirmInfoPage.prototype.setMunicipio = function (event) {
        var _this = this;
        this.colonias = [];
        this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/localidades?municipio_id=' + event)
            .toPromise()
            .then(function (data) {
            _this.datos2 = data;
            _this.colonias = _this.datos2.localidades;
            _this.registerUserForm.patchValue({ localidad_id: _this.colonias[0].id });
        }, function (msg) {
            _this.presentToast('No se pudo cargar las colonias, intenta de nuevo', 1500);
        });
    };
    ConfirmInfoPage.prototype.setColonia = function (event) {
        this.registerUserForm.patchValue({ localidad_id: event });
    };
    ConfirmInfoPage.prototype.presentModal = function () {
        var _this = this;
        if (this.dataPreference != '') {
            var profileModal = this.modalCtrl.create(ModalPreferencePage, { prefer: this.preferences, collec: this.dataPreference.categorias });
            profileModal.onDidDismiss(function (data) {
                _this.preferences = data;
            });
            profileModal.present();
        }
        else {
            this.presentToast('No se pudieron cargar las preferencias ingresa de nuevo', 1500);
        }
    };
    ConfirmInfoPage.prototype.deleteChip = function (prefer) {
        var index = this.preferences.findIndex(function (item1) { return item1.nombre === prefer.nombre; });
        if (index !== -1) {
            this.preferences.splice(index, 1);
        }
        for (var i = 0; i < this.dataPreference.categorias.length; ++i) {
            if (this.dataPreference.categorias[i].nombre == prefer.nombre) {
                this.dataPreference.categorias[i].checked = false;
            }
        }
    };
    ConfirmInfoPage.prototype.register = function () {
        var _this = this;
        this.registerUserForm.value.preferencias = JSON.stringify(this.preferences);
        this.registerUserForm.value.email = this.registerUserForm.value.email.toLowerCase();
        if (this.registerUserForm.valid) {
            this.showLoading();
            this.auth.registerSocial(this.registerUserForm.value).subscribe(function (success) {
                if (success) {
                    _this.loading.dismiss();
                    _this.createSuccess = true;
                    _this.showPopup("Completado", 'Usuario registrado con éxito');
                }
                else {
                    _this.presentToast("Ha ocurrido un error al crear la cuenta.", 1500);
                }
            }, function (error) {
                _this.loading.dismiss();
                _this.showPopup("Error", error.error);
            });
        }
        else {
            this.validateAllFormFields(this.registerUserForm);
            this.presentToast('¡Faltan datos para el registro!', 1500);
        }
    };
    ConfirmInfoPage.prototype.onValueChanged = function (data) {
        if (!this.registerUserForm) {
            return;
        }
        var form = this.registerUserForm;
        for (var field in this.formErrors) {
            var control = form.get(field);
            this.formErrors[field] = '';
            if (control && control.dirty && !control.valid) {
                for (var key in control.errors) {
                    this.formErrors[field] += true;
                    console.log(key);
                }
            }
        }
    };
    ConfirmInfoPage.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsDirty({ onlySelf: true });
                _this.onValueChanged();
            }
            else if (control instanceof FormGroup) {
                _this.validateAllFormFields(control);
            }
        });
    };
    ConfirmInfoPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Registrando usuario...',
            spinner: 'ios',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    ConfirmInfoPage.prototype.showLoadingc = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Cargando...',
            spinner: 'ios'
        });
        this.loading.present();
    };
    ConfirmInfoPage.prototype.showPopup = function (title, text) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons: [
                {
                    text: 'OK',
                    handler: function (data) {
                        if (_this.createSuccess) {
                            _this.navCtrl.setRoot(TabsPage);
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    ConfirmInfoPage.prototype.presentToast = function (text, time) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: time,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    ConfirmInfoPage = __decorate([
        Component({
            selector: 'page-confirm-info',
            templateUrl: 'confirm-info.html',
        }),
        __metadata("design:paramtypes", [NavController, AuthServiceProvider, NavParams, HttpClient, LoadingController, FormBuilder, AlertController, ToastController, RuteBaseProvider, ModalController])
    ], ConfirmInfoPage);
    return ConfirmInfoPage;
}());
export { ConfirmInfoPage };
//# sourceMappingURL=confirm-info.js.map