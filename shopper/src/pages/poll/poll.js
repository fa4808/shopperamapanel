var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ElementRef, NgZone } from '@angular/core';
import { NavController, NavParams, App, LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { ElementsServicesProvider } from '../../providers/elements-services/elements-services';
import { LoginPage } from '../login/login';
import { Camera } from '@ionic-native/camera';
import { NgProgress } from '@ngx-progressbar/core';
import { OcrProvider } from '../../providers/ocr/ocr';
import { Events } from 'ionic-angular';
import { WizardAnimations } from '../../components/wizard/ion-simple-wizard-animations';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
var PollPage = /** @class */ (function () {
    function PollPage(navCtrl, navParams, element, http, rutebaseAPI, elementService, app, camera, progress, loadingCtrl, ocr, evts, photoViewer, file, transfer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.element = element;
        this.http = http;
        this.rutebaseAPI = rutebaseAPI;
        this.elementService = elementService;
        this.app = app;
        this.camera = camera;
        this.progress = progress;
        this.loadingCtrl = loadingCtrl;
        this.ocr = ocr;
        this.evts = evts;
        this.photoViewer = photoViewer;
        this.file = file;
        this.transfer = transfer;
        this.poll = [];
        this.poll_name = '';
        this.poll_question = [];
        this.logo_sucursal = '';
        this.nombre_sucursal = '';
        this.selectedImage = 'assets/imgs/ticket-view.jpg';
        this.selectedImage1 = 'assets/imgs/price.png';
        this.imageText = '';
        this.lastImage = null;
        this.respuesta = '';
        this.mount_payment = '';
        this.sucursales = [];
        this.count_cuest = 0;
        this.create_respuesta = {
            cuestionario: '',
            imagen_factura: '',
            estado_pagado: 1,
            monto_pagado: '',
            campana_id: '',
            sucursal_id: '',
            cliente_id: '',
            cuestionario_id: '',
            empresa_id: ''
        };
        this.imgUrl = 'assets/imgs/ticket-view.jpg';
        this.logo_sucursal = this.navParams.get('logo');
        this.nombre_sucursal = this.navParams.get('nombre');
        this.cuestionario_id = this.navParams.get('cuestionario_id');
        this.create_respuesta.cuestionario_id = this.cuestionario_id;
        this.create_respuesta.campana_id = this.navParams.get('campana_id');
        this.create_respuesta.cliente_id = this.navParams.get('client_id');
        this.initQuestion();
        this._zone = new NgZone({ enableLongStackTrace: false });
        this.step = 1;
        this.stepCondition = false;
        this.stepDefaultCondition = this.stepCondition;
        this.evts.subscribe('step:changed', function (step) {
            _this.currentStep = step;
            _this.stepCondition = _this.stepDefaultCondition;
        });
        this.evts.subscribe('step:next', function () {
            if (_this.currentStep == 2) {
                if (_this.create_respuesta.imagen_factura == '') {
                    _this.stepCondition = false;
                }
                else {
                    _this.stepCondition = true;
                }
            }
            if (_this.currentStep == 3) {
                if (_this.imageText == '') {
                    _this.stepCondition = false;
                }
                else {
                    _this.stepCondition = true;
                }
            }
            if (_this.currentStep == 4) {
                if (_this.mount_payment == '') {
                    _this.stepCondition = false;
                }
                else {
                    _this.stepCondition = true;
                }
            }
            if (_this.currentStep == 5) {
                _this.stepCondition = true;
            }
        });
        this.evts.subscribe('step:back', function () {
            _this.stepCondition = true;
        });
    }
    PollPage.prototype.initQuestion = function () {
        var _this = this;
        this.elementService.showLoading('Cargando Evaluación...');
        this.http.get(this.rutebaseAPI.getRutaApi() + 'cuestionarios/' + this.cuestionario_id)
            .toPromise()
            .then(function (data) {
            _this.datos = data;
            console.log(data);
            _this.poll = JSON.parse(_this.datos.cuestionario.cuestionario);
            _this.poll_name = _this.poll.nombre;
            _this.poll_question = _this.poll.preguntas;
            _this.create_respuesta.empresa_id = _this.datos.cuestionario.campana.empresa_id;
            _this.sucursales = _this.datos.cuestionario.campana.sucursales;
            _this.create_respuesta.sucursal_id = _this.sucursales[0].id;
            for (var i = 0; i < _this.poll_question.length; ++i) {
                _this.poll_question[i].rpta = '';
            }
            _this.elementService.hideLoading();
        }, function (msg) {
            _this.elementService.hideLoading();
            if (msg.status == 400 || msg.status == 401) {
                _this.elementService.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
                _this.app.getRootNav().setRoot(LoginPage);
            }
            else {
                _this.elementService.presentToast(msg.error.error);
            }
        });
    };
    PollPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            quality: 50,
            sourceType: this.camera.PictureSourceType.CAMERA,
            saveToPhotoAlbum: true,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
        }, function (err) {
            _this.elementService.presentToast('Error al seleccionar la imagen');
            //this.stepCondition = true;//
        });
    };
    // Create a new name for the image
    PollPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    PollPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            _this.uploadImage();
        }, function (error) {
            _this.elementService.presentToast('Error al guardar la imagen');
        });
    };
    // Always get the accurate path to your apps folder
    PollPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    PollPage.prototype.uploadImage = function () {
        var _this = this;
        // Destination URL
        var url = "https://api.shopperama.mx/images_uploads/upload.php";
        // File for Upload
        var targetPath = this.pathForImage(this.lastImage);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename }
        };
        var fileTransfer = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'Subiendo Imagen...',
            spinner: 'ios'
        });
        this.loading.present();
        // Use the FileTransfer to upload the image
        fileTransfer.upload(targetPath, url, options).then(function (data) {
            _this.loading.dismissAll();
            _this.selectedImage = targetPath;
            _this.create_respuesta.imagen_factura = data.response;
            _this.stepCondition = true;
        }, function (err) {
            _this.loading.dismissAll();
            _this.elementService.presentToast('Error al subir la imagen');
            _this.selectedImage = 'assets/imgs/ticket-view.jpg';
        });
    };
    PollPage.prototype.getPicture1 = function () {
        var _this = this;
        this.camera.getPicture({
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false,
            correctOrientation: true
        }).then(function (imageData) {
            _this.crop.crop(imageData, { quality: 100 })
                .then(function (newImage) {
                _this.selectedImage1 = newImage;
                _this.toBase64(newImage).then(function (base64Img) {
                    _this.loading = _this.loadingCtrl.create({
                        content: 'Verificando monto...',
                        spinner: 'ios'
                    });
                    _this.loading.present();
                    var that = _this;
                    _this.ocr.recognizeText(base64Img).then(function (result) {
                        that._zone.run(function () {
                            if (result != '') {
                                that.imageText = result;
                                that.create_respuesta.monto_pagado = result;
                                that.stepCondition = true;
                                that.loading.dismissAll();
                            }
                            else {
                                that.loading.dismissAll();
                                that.stepCondition = false;
                                that.elementService.presentToast('No hemos podido obtener el monto del ticket, por favor sube una imagen más visible');
                            }
                        });
                    });
                });
            }, function (error) { return console.error('Error cropping image', error); });
        }, function (err) {
            console.log(err);
            _this.elementService.presentToast('Error al seleccionar la imagen');
            //this.stepCondition = true;//
        });
    };
    PollPage.prototype.toBase64 = function (url) {
        return new Promise(function (resolve) {
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'blob';
            xhr.onload = function () {
                var reader = new FileReader();
                reader.onloadend = function () {
                    resolve(reader.result);
                };
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.send();
        });
    };
    ;
    PollPage.prototype.viewReferenceTicket = function () {
        this.photoViewer.show(this.file.applicationDirectory + 'www/assets/imgs/ticket.jpg');
    };
    PollPage.prototype.viewREference = function () {
        this.photoViewer.show(this.file.applicationDirectory + 'www/assets/imgs/ticket-price.png');
    };
    PollPage.prototype.onFinish = function () {
        var _this = this;
        var count = 0;
        for (var i = 0; i < this.poll_question.length; ++i) {
            if (this.poll_question[i].rpta == '') {
                count = 1;
            }
        }
        if (count == 1) {
            this.elementService.presentToast('Debes completar todas las preguntas del cuestionario');
            return true;
        }
        if (this.create_respuesta.imagen_factura == '') {
            this.elementService.presentToast('Debes subir una imagen del ticket');
            return true;
        }
        if (this.imageText == '') {
            this.elementService.presentToast('Debes subir una imagen del monto del ticket');
            return true;
        }
        this.create_respuesta.cuestionario = JSON.stringify(this.poll_question);
        console.log(this.create_respuesta);
        this.loading = this.loadingCtrl.create({
            content: 'Verificando información del ticket y el comercio, por favor espere...',
            spinner: 'ios'
        });
        this.loading.present();
        this.http.post(this.rutebaseAPI.getRutaApi() + 'evaluaciones', this.create_respuesta)
            .toPromise()
            .then(function (data) {
            _this.loading.dismissAll();
            _this.elementService.presentToast('Su encuesta ha sido enviada con éxito');
            _this.navCtrl.popToRoot();
        }, function (msg) {
            _this.loading.dismissAll();
            console.log(msg.error);
            _this.elementService.presentToast(msg.error);
        });
    };
    PollPage.prototype.changeQuestion = function (item) {
        var count = 0;
        for (var i = 0; i < this.poll_question.length; ++i) {
            if (this.poll_question[i].rpta == '') {
                count = +1;
            }
        }
        if (count > 0) {
            this.stepCondition = false;
        }
        else {
            this.stepCondition = true;
        }
    };
    PollPage.prototype.changeMount = function (mount_payment) {
        if (mount_payment == '') {
            this.stepCondition = false;
        }
        else {
            this.create_respuesta.monto_pagado = mount_payment;
            this.stepCondition = true;
        }
        /*console.log(this.create_respuesta.monto_pagado.toString());
        console.log(this.imageText.toString());
        if (parseFloat(this.create_respuesta.monto_pagado) != parseFloat(this.imageText.toString())) {
          this.elementService.presentToast('El monto ingresado no coincide con el monto registrado del ticket');
          return true;
        }*/
    };
    PollPage = __decorate([
        Component({
            selector: 'page-poll',
            templateUrl: 'poll.html',
            animations: WizardAnimations.btnRotate
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            ElementRef,
            HttpClient,
            RuteBaseProvider,
            ElementsServicesProvider,
            App,
            Camera,
            NgProgress,
            LoadingController,
            OcrProvider,
            Events,
            PhotoViewer,
            File,
            Transfer])
    ], PollPage);
    return PollPage;
}());
export { PollPage };
//# sourceMappingURL=poll.js.map