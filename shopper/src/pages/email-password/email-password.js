var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CodepasswordPage } from '../codepassword/codepassword';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
var EmailPasswordPage = /** @class */ (function () {
    function EmailPasswordPage(navCtrl, navParams, http, loadingCtrl, builder, rutebaseAPI, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.builder = builder;
        this.rutebaseAPI = rutebaseAPI;
        this.toastCtrl = toastCtrl;
        this.createSuccess = false;
        this.formErrors = {
            'email': ''
        };
        this.validationMessages = {
            'email': {
                'required': 'El correo es requerido.',
                'email': 'El formato del correo no es correcto.',
            }
        };
        this.ForgotPassword = this.builder.group({
            email: ['', [Validators.required, Validators.email]]
        });
        this.ForgotPassword.valueChanges.subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged();
    }
    EmailPasswordPage.prototype.sendPassword = function () {
        var _this = this;
        if (this.ForgotPassword.valid) {
            this.showLoading();
            this.http.get(this.rutebaseAPI.getRutaApi() + 'password/cliente/' + this.ForgotPassword.value.email)
                .toPromise()
                .then(function (data) {
                _this.codigo = data;
                _this.loading.dismiss();
                _this.presentToast('Código de verificación enviado con éxito');
                _this.navCtrl.push(CodepasswordPage, { email: _this.ForgotPassword.value.email });
            }, function (msg) {
                var err = msg.error;
                _this.loading.dismiss();
                _this.presentToast(err.error);
            });
        }
        else {
            this.validateAllFormFields(this.ForgotPassword);
        }
    };
    EmailPasswordPage.prototype.codepage = function () {
        this.navCtrl.push(CodepasswordPage, { email: this.ForgotPassword.value.email });
    };
    EmailPasswordPage.prototype.onValueChanged = function (data) {
        if (!this.ForgotPassword) {
            return;
        }
        var form = this.ForgotPassword;
        for (var field in this.formErrors) {
            var control = form.get(field);
            this.formErrors[field] = '';
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    };
    EmailPasswordPage.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsDirty({ onlySelf: true });
                _this.onValueChanged();
            }
            else if (control instanceof FormGroup) {
                _this.validateAllFormFields(control);
            }
        });
    };
    EmailPasswordPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Enviando código...',
            spinner: 'ios',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    EmailPasswordPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    EmailPasswordPage = __decorate([
        Component({
            selector: 'page-email-password',
            templateUrl: 'email-password.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, HttpClient, LoadingController, FormBuilder, RuteBaseProvider, ToastController])
    ], EmailPasswordPage);
    return EmailPasswordPage;
}());
export { EmailPasswordPage };
//# sourceMappingURL=email-password.js.map