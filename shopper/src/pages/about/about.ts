import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { LoginPage } from '../login/login';
import {StorageProvider} from '../../providers/storage/storage';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController, public app: App, public storage: StorageProvider) {

  }

  	logout(){
		this.storage.set('tokenShopper','');
		this.storage.setObject('userShopper', '');
		this.app.getRootNav().setRoot(LoginPage);
	}

}
