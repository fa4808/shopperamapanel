var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ContrasenaPage } from '../contrasena/contrasena';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
var CodepasswordPage = /** @class */ (function () {
    function CodepasswordPage(navCtrl, navParams, http, loadingCtrl, builder, rutebaseAPI, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.builder = builder;
        this.rutebaseAPI = rutebaseAPI;
        this.toastCtrl = toastCtrl;
        this.formErrors = {
            'codigo': ''
        };
        this.validationMessages = {
            'codigo': {
                'required': 'El código de verificacion es requerido.'
            }
        };
        this.correo = navParams.get('email');
        this.CodepasswordForm = this.builder.group({
            codigo: ['', [Validators.required]]
        });
        this.CodepasswordForm.valueChanges.subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged();
    }
    CodepasswordPage.prototype.codePassword = function () {
        /*if (this.CodepasswordForm.valid) {
            this.showLoading();
            this.http.get(this.rutebaseAPI.getRutaApi()+'password/codigo/'+this.CodepasswordForm.value.codigo)
            .toPromise()
            .then(
                data => {
                    this.id = data;
                    this.loading.dismiss();
                    this.navCtrl.push(ContrasenaPage, {id_cliente: this.id.cliente_id, token: this.id.token});
                },
                msg => {
                    let err = msg.error;
                    this.loading.dismiss();
                    this.presentToast(err.error);
                });
        } else {
          this.validateAllFormFields(this.CodepasswordForm);
        }*/
        this.navCtrl.push(ContrasenaPage);
    };
    CodepasswordPage.prototype.solicitar = function () {
        this.navCtrl.pop();
    };
    CodepasswordPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Verificando código...',
            spinner: 'ios',
            duration: 25000,
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    CodepasswordPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    CodepasswordPage.prototype.onValueChanged = function (data) {
        if (!this.CodepasswordForm) {
            return;
        }
        var form = this.CodepasswordForm;
        for (var field in this.formErrors) {
            var control = form.get(field);
            this.formErrors[field] = '';
            if (control && control.dirty && !control.valid) {
                var messages = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    };
    CodepasswordPage.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsDirty({ onlySelf: true });
                _this.onValueChanged();
            }
            else if (control instanceof FormGroup) {
                _this.validateAllFormFields(control);
            }
        });
    };
    CodepasswordPage = __decorate([
        Component({
            selector: 'page-codepassword',
            templateUrl: 'codepassword.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, HttpClient, LoadingController, FormBuilder, RuteBaseProvider, ToastController])
    ], CodepasswordPage);
    return CodepasswordPage;
}());
export { CodepasswordPage };
//# sourceMappingURL=codepassword.js.map