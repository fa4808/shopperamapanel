var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ModalPreferencePage } from '../pages/modal-preference/modal-preference';
import { ConfirmInfoPage } from '../pages/confirm-info/confirm-info';
import { EvaluationsPendingPage } from '../pages/evaluations-pending/evaluations-pending';
import { AcceptQuestionPage } from '../pages/accept-question/accept-question';
import { PollPage } from '../pages/poll/poll';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { EvaluationDetailPage } from '../pages/evaluation-detail/evaluation-detail';
import { WalletPage } from '../pages/wallet/wallet';
import { ModalAccountPage } from '../pages/modal-account/modal-account';
import { EmailPasswordPage } from '../pages/email-password/email-password';
import { CodepasswordPage } from '../pages/codepassword/codepassword';
import { ContrasenaPage } from '../pages/contrasena/contrasena';
import { Facebook } from '@ionic-native/facebook';
import { TwitterConnect } from '@ionic-native/twitter-connect';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { StorageProvider } from '../providers/storage/storage';
import { RuteBaseProvider } from '../providers/rute-base/rute-base';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { OneSignal } from '@ionic-native/onesignal';
import { ElementsServicesProvider } from '../providers/elements-services/elements-services';
import { Camera } from '@ionic-native/camera';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { CarouselComponent } from "../components/carousel/carousel";
import { OcrProvider } from '../providers/ocr/ocr';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Transfer } from '@ionic-native/transfer';
import { IonSimpleWizard } from '../components/wizard/ion-simple-wizard.component';
import { IonSimpleWizardStep } from '../components/wizard/ion-simple-wizard.step.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { KeysPipe } from '../pipes/groupdate/groupdate';
import { Keyboard } from '@ionic-native/keyboard';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                AboutPage,
                ContactPage,
                HomePage,
                TabsPage,
                LoginPage,
                RegisterPage,
                ModalPreferencePage,
                ConfirmInfoPage,
                EvaluationsPendingPage,
                AcceptQuestionPage,
                PollPage,
                CarouselComponent,
                IonSimpleWizard,
                IonSimpleWizardStep,
                EditProfilePage,
                EvaluationDetailPage,
                KeysPipe,
                WalletPage,
                ModalAccountPage,
                EmailPasswordPage,
                CodepasswordPage,
                ContrasenaPage
            ],
            imports: [
                BrowserModule,
                BrowserAnimationsModule,
                HttpClientModule,
                HttpModule,
                ReactiveFormsModule,
                AngularFireModule.initializeApp(environment.firebase),
                AngularFireAuthModule,
                NgProgressModule.forRoot(),
                NgProgressHttpModule,
                IonicModule.forRoot(MyApp, {
                    tabsPlacement: 'bottom',
                    backButtonText: ''
                })
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                AboutPage,
                ContactPage,
                HomePage,
                TabsPage,
                LoginPage,
                RegisterPage,
                ModalPreferencePage,
                ConfirmInfoPage,
                EvaluationsPendingPage,
                AcceptQuestionPage,
                PollPage,
                CarouselComponent,
                EditProfilePage,
                EvaluationDetailPage,
                WalletPage,
                ModalAccountPage,
                EmailPasswordPage,
                CodepasswordPage,
                ContrasenaPage
            ],
            providers: [
                StatusBar,
                SplashScreen,
                Facebook,
                TwitterConnect,
                AngularFireAuth,
                { provide: ErrorHandler, useClass: IonicErrorHandler },
                AuthServiceProvider,
                StorageProvider,
                RuteBaseProvider,
                OneSignal,
                ElementsServicesProvider,
                Camera,
                OcrProvider,
                File,
                FilePath,
                Transfer,
                PhotoViewer,
                Keyboard
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map