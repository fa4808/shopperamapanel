import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, IonicApp, App, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { StorageProvider } from '../providers/storage/storage';
import { HttpClient } from '@angular/common/http';
import { RuteBaseProvider } from '../providers/rute-base/rute-base';
import { Keyboard } from '@ionic-native/keyboard';
import { HelpPage } from '../pages/help/help';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = LoginPage;

  constructor(platform: Platform, public events: Events, private ionicApp: IonicApp, statusBar: StatusBar, splashScreen: SplashScreen, public storage: StorageProvider, public http: HttpClient, public rutebaseAPI: RuteBaseProvider, public app: App, private keyboard: Keyboard) {
    var item = this.storage.get('tokenShopper');
    if (item !== null && item !== "") {
      this.rootPage = TabsPage;
    } else {
      this.rootPage = LoginPage;
    }
    platform.ready().then(() => {
      
      statusBar.styleLightContent(); 
      setTimeout(()=>{
        splashScreen.hide();  
      },1000);

      keyboard.onKeyboardShow().subscribe(() => {
        document.body.classList.add('keyboard-is-open');
      });
      keyboard.onKeyboardHide().subscribe(() => {
        document.body.classList.remove('keyboard-is-open');
      });

      let OneSignal = window["plugins"].OneSignal;
      var that = this;

      OneSignal
      .startInit("32500e5f-2def-4a7a-96bf-541ceb67df76", "275279573191")
      .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
      .handleNotificationOpened(function(jsonData) {

        //if(jsonData.notification.payload.additionalData.accion == 1){
          //var sucursal = JSON.parse(jsonData.notification.payload.additionalData.obj);
          //var cuestionario_id = jsonData.notification.payload.additionalData.cuestionario_id;
          //that.nav.push(AcceptQuestionPage, {cuestionario_id: cuestionario_id, nombre: sucursal.sucursal, logo: sucursal.logo});      
        //}
        if(jsonData.notification.payload.additionalData.accion == 2){
          var user = JSON.parse(jsonData.notification.payload.additionalData.obj);
          that.nav.push(HelpPage, {admin_id: user.emisor.id, chat_id: user.chat_id, token_notificacion: user.emisor.token_notificacion});
        }
      })
      .handleNotificationReceived(function(jsonData) {

        //if(jsonData.payload.additionalData.accion == 1){
          //var sucursal = JSON.parse(jsonData.payload.additionalData.obj);
          //var cuestionario_id = jsonData.payload.additionalData.cuestionario_id;
          //that.nav.push(AcceptQuestionPage, {cuestionario_id: cuestionario_id, nombre: sucursal.sucursal, logo: sucursal.logo});       
        //}

        if(jsonData.payload.additionalData.accion == 2){          
          var user = JSON.parse(jsonData.payload.additionalData.obj);
          const mockMsg = {
            id: Date.now().toString(),
            emisor_id: user.emisor.id,
            userAvatar: user.emisor.imagen,
            receptor_id: 232322,
            created_at: Date.now(),
            msg: jsonData.payload.additionalData.contenido,
            status: 2
          };
          that.events.publish('chat:received', mockMsg, Date.now())
        }
      })
      .endInit();
    });

    platform.registerBackButtonAction(() => {
      let nav = app.getActiveNavs()[0];
      let activePortal = this.ionicApp._loadingPortal.getActive() ||
        this.ionicApp._modalPortal.getActive() ||
        this.ionicApp._toastPortal.getActive() ||
        this.ionicApp._overlayPortal.getActive();
      if (activePortal) {
        activePortal.instance.backButtonAction();
        console.log('entro');
        return;
      } else if(nav.canGoBack()){
        nav.pop();
        return;
      } else {
        platform.exitApp();   
        return;
      }
    });
  }
}
