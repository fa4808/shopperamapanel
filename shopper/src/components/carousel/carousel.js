var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';
var CarouselComponent = /** @class */ (function () {
    function CarouselComponent(eleRef) {
        this.eleRef = eleRef;
        this.currentDeg = 0;
        this.items = [];
        this.selectSlide = new EventEmitter();
    }
    Object.defineProperty(CarouselComponent.prototype, "slides", {
        set: function (values) {
            if (!values.length)
                return;
            var degree = 0;
            this.tz = 200; //Math.round((this.containerWidth / 2) /
            //Math.tan(Math.PI / values.length));
            setTimeout(function () {
                this.items = values.map(function (item, index) {
                    var slideItem = {
                        idx: index,
                        cuestionario_id: item.cuestionario_id,
                        campana_id: item.campana_id,
                        nombre_empresa: item.nombre_empresa,
                        empresa_imagen: item.empresa_imagen,
                        currentPlacement: degree,
                        nombre_campana: item.nombre_campana
                    };
                    degree = degree + 60;
                    console.log(slideItem);
                    return slideItem;
                });
            }, 5000);
        },
        enumerable: true,
        configurable: true
    });
    CarouselComponent.prototype.onSwipeLeft = function (event, item) {
        if (item.idx != this.items.length - 1) {
            this.currentDeg = this.currentDeg - 60;
            this.applyStyle();
        }
    };
    CarouselComponent.prototype.onSwipeRight = function (event, item) {
        if (item.idx != 0) {
            this.currentDeg = this.currentDeg + 60;
            this.applyStyle();
        }
    };
    CarouselComponent.prototype.applyStyle = function () {
        var ele = this.eleRef.nativeElement.querySelector('.carousel');
        ele.style['-webkit-transform'] = "rotateY(" + this.currentDeg + "deg)";
        ele.style['-moz-transform'] = "rotateY(" + this.currentDeg + "deg)";
        ele.style['-o-transform'] = "rotateY(" + this.currentDeg + "deg)";
        ele.style['transform'] = "rotateY(" + this.currentDeg + "deg)";
    };
    CarouselComponent.prototype.selectItem = function (item) {
        this.selectSlide.emit(item);
    };
    __decorate([
        Input(),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], CarouselComponent.prototype, "slides", null);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], CarouselComponent.prototype, "selectSlide", void 0);
    CarouselComponent = __decorate([
        Component({
            selector: 'carousel',
            templateUrl: 'carousel.html'
        }),
        __metadata("design:paramtypes", [ElementRef])
    ], CarouselComponent);
    return CarouselComponent;
}());
export { CarouselComponent };
//# sourceMappingURL=carousel.js.map