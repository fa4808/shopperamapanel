import { Injectable } from '@angular/core';

@Injectable()
export class RutaService {

  //public ruta_servidor = "https://api.shopperama.mx/";
  public ruta_servidor = "http://shopper.internow.com.mx/shoppersAPI/public/";

  public ruta_open = "https://sandbox-api.openpay.mx/v1/mvrmd6am0rl7nc4vts41/";

  //public images_base = 'http://shopper.internow.com.mx/shoppersAPI/public/images_uploads/';
  public images_base = 'http://shopper.internow.com.mx/images_uploads/';

  //public images_flats = 'http://shopper.internow.com.mx/shoppersAPI/public/images/';
  public images_flats = 'http://shopper.internow.com.mx/images/';

  constructor() { }

  get_ruta(){
  	return this.ruta_servidor;
  }

  get_open(){
  	return this.ruta_open;
  }

  get_ruta_images(){
		return this.images_base;
	}

  get_ruta_images_flats(){
		return this.images_flats;
	}

}
